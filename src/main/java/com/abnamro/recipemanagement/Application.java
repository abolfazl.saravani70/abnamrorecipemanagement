package com.abnamro.recipemanagement;

import com.abnamro.recipemanagement.index.Indexer;
import com.abnamro.recipemanagement.model.Recipe;
import com.abnamro.recipemanagement.repository.RecipeRepository;
import com.abnamro.recipemanagement.model.Ingredient;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public ApplicationRunner initializeData(RecipeRepository recipeRepository) throws Exception {
		return (ApplicationArguments args) -> {
			Recipe recipeOne = new Recipe();
			recipeOne.setName("Boiled Eggs");
			recipeOne.setInstruction("Place your eggs in a pot and cover with cold water by 1 inch. Bring to a boil over medium-high heat, then cover, remove from the heat and set aside 8 to 10 minutes. Drain, cool in ice water and peel.");
			recipeOne.setServingSize(1);
			recipeOne.setIsVegetarian(true);

			Recipe recipeTwo = new Recipe();
			recipeTwo.setName("Egg Salad");
			recipeTwo.setServingSize(2);
			recipeTwo.setIsVegetarian(true);
			recipeTwo.setInstruction("Place egg in a saucepan and cover with cold water. Bring water to a boil and immediately remove from heat. Cover and let eggs stand in hot water for 10 to 12 minutes. Remove from hot water, cool, peel and chop. " +
					"Place the chopped eggs in a bowl, and stir in the mayonnaise, mustard and green onion. Season with salt, pepper and paprika. Stir and serve on your favorite bread or crackers.");


			Recipe recipeThree = new Recipe();
			recipeThree.setName("Raw chicken");
			recipeThree.setServingSize(4);
			recipeThree.setIsVegetarian(false);
			recipeThree.setInstruction("Defrost the chicken you freshly bought from AH Jumbo. Wash the chicken thoroughly. Then eat one bite at a time with a disgusted face.");



			Ingredient ingredientOne = new Ingredient();
			ingredientOne.setName("Egg");

			Ingredient ingredientTwo = new Ingredient();
			ingredientTwo.setName("Lattice");
			Ingredient ingredientThree = new Ingredient();
			ingredientThree.setName("Chicken");

			recipeOne.getIngredients().add(ingredientOne);
			recipeTwo.getIngredients().add(ingredientOne);
			recipeTwo.getIngredients().add(ingredientTwo);
			recipeThree.getIngredients().add(ingredientThree);

			recipeRepository.saveAllAndFlush(Arrays.asList(recipeTwo,recipeOne,recipeThree));

		};
	}

	@Bean
	public ApplicationRunner buildIndex(Indexer indexer) {
		return (ApplicationArguments args) -> {
			indexer.indexPersistedData("com.abnamro.recipemanagement.model.Recipe");
			indexer.indexPersistedData("com.abnamro.recipemanagement.model.Ingredient");
		};
	}

}
