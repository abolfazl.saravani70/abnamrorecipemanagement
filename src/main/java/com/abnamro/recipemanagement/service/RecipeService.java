package com.abnamro.recipemanagement.service;

import com.abnamro.recipemanagement.model.Recipe;
import com.abnamro.recipemanagement.index.dto.RecipeDTO;
import com.abnamro.recipemanagement.index.dto.search.SearchParameter;
import com.abnamro.recipemanagement.repository.RecipeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class RecipeService {
    RecipeRepository recipeRepository;

    public boolean createRecipe(RecipeDTO recipeDTO){
        boolean isSuccessful = false;
        try{
            Recipe recipeToBeAdded = new Recipe();
            recipeToBeAdded.map(recipeDTO);
            isSuccessful = true;
        }catch (Exception e){
            log.error("Error adding recipe.",e);
            isSuccessful = false;
        }
        return isSuccessful;
    }

    public boolean updateRecipe(RecipeDTO recipeDTO){
        boolean isSuccessful = false;
        try {
            Recipe recipe = recipeRepository.getById(recipeDTO.getId());
            recipe.map(recipeDTO);
            isSuccessful = true;
        }catch (Exception e){
            log.error("Error updating recipe.",e);
            isSuccessful = false;
        }
        return isSuccessful;
    }

    public void deleteRecipe(RecipeDTO recipeDTO){
        boolean isSucessful = false;
        try{
            recipeRepository.deleteById(recipeDTO.getId());
            isSucessful = true;
        }catch (Exception e){
            log.error("Error deleting recipe.",e);
            isSucessful = false;
        }
    }

    public List<RecipeDTO> findAll(){
        List<RecipeDTO> result = new ArrayList<>();
        recipeRepository.findAll().stream().forEach(recipe->{
            RecipeDTO recipeDTO = new RecipeDTO();
            recipeDTO.map(recipe);
            result.add(recipeDTO);
        });

        return result;
    }


    public List<RecipeDTO> searchByMultipleFilters(List<SearchParameter> searchParameters, int limit) {
        List<Recipe> searchResult = recipeRepository.multipleFiltersSearch(searchParameters,limit).hits();
        List<RecipeDTO> result = new ArrayList<>();
        searchResult.stream().forEach(recipe -> {
            RecipeDTO recipeDTO = new RecipeDTO();
            recipeDTO.map(recipe);
            result.add(recipeDTO);
        });
        return result;
    }
}
