package com.abnamro.recipemanagement.controller;

import com.abnamro.recipemanagement.index.dto.RecipeDTO;
import com.abnamro.recipemanagement.index.dto.search.SearchParameter;
import com.abnamro.recipemanagement.service.RecipeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Slf4j
@RestController
@RequestMapping("/recipes")
@AllArgsConstructor
public class RecipeController {

    RecipeService recipeService;


    @GetMapping
    public List<RecipeDTO> findAll(){
        return recipeService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createRecipe(RecipeDTO recipeDTO){
        recipeService.createRecipe(recipeDTO);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteRecipe(RecipeDTO recipeDTO){
        recipeService.deleteRecipe(recipeDTO);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateRecipe(RecipeDTO recipeDTO){
        recipeService.updateRecipe(recipeDTO);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.FOUND)
    public List<RecipeDTO> search(@RequestBody List<SearchParameter> searchParameters){
         int limit= 100;
        List<RecipeDTO> result=  recipeService.searchByMultipleFilters(searchParameters,limit);

        return result;
    }

}
