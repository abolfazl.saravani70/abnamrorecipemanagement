package com.abnamro.recipemanagement.repository;

import com.abnamro.recipemanagement.model.Recipe;
import org.springframework.stereotype.Repository;


@Repository
public interface RecipeRepository extends SearchRepository<Recipe,Long>{


}
