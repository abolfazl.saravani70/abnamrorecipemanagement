package com.abnamro.recipemanagement.repository;

import com.abnamro.recipemanagement.model.Ingredient;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends SearchRepository<Ingredient,Long>{
}
