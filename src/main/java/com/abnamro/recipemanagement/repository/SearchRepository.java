package com.abnamro.recipemanagement.repository;

import com.abnamro.recipemanagement.index.dto.search.SearchParameter;
import org.hibernate.search.engine.search.query.SearchResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface SearchRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

    //SearchResult<T> searchBy(String text, int limit, String... fields);
    /**
     * This method is override inside implementation.
     * @param searchParameters
     * @param limit
     * @return
     */
    SearchResult<T> multipleFiltersSearch(List<SearchParameter> searchParameters, int limit);

}
