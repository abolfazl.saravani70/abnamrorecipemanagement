package com.abnamro.recipemanagement.index.dto.search;

import com.abnamro.recipemanagement.model.Recipe;
import org.hibernate.search.engine.search.predicate.SearchPredicate;
import org.hibernate.search.mapper.orm.scope.SearchScope;


public class IntegerSearchParameter extends SearchParameter<Integer,Recipe>{


    public IntegerSearchParameter(String name, Integer value, Boolean mustHold) {
        this.name = name;
        this.value = value;
        this.mustHold = mustHold;
    }



    @Override
    public SearchPredicate applyFilter(SearchScope<Recipe> searchScope) {
        return searchScope.predicate().match().field(getName()).matching(getValue()).toPredicate();
        //return searchScope.predicate().range().field(getName()).between(getValue(),getValue()).toPredicate();
    }
}
