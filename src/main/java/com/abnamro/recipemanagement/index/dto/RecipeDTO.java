package com.abnamro.recipemanagement.index.dto;


import com.abnamro.recipemanagement.model.Recipe;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
public class RecipeDTO {

    private Long id;

    private Integer servingSize;

    private String name;

    private String instruction;

    private Boolean isVegetarian;

    private Set<IngredientDTO> ingredients = new HashSet<>();


    public void map(Recipe recipe){
        this.setId(recipe.getId());
        this.setName(recipe.getName());
        this.setInstruction(recipe.getInstruction());
        this.setIsVegetarian(recipe.getIsVegetarian());
        this.setServingSize(recipe.getServingSize());
        recipe.getIngredients().stream().forEach(x->{
            IngredientDTO ingredientDTO = new IngredientDTO();
            ingredientDTO.map(x);
            this.getIngredients().add(ingredientDTO);
        });

    }
}
