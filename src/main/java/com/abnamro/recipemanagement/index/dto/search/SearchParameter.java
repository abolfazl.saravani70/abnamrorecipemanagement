package com.abnamro.recipemanagement.index.dto.search;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.search.engine.search.predicate.SearchPredicate;
import org.hibernate.search.mapper.orm.scope.SearchScope;

@Setter
@Getter
public abstract class SearchParameter<T,R> {
    String name;
    T value;
    Boolean mustHold;

    public abstract SearchPredicate applyFilter(SearchScope<R> searchScope);

}
