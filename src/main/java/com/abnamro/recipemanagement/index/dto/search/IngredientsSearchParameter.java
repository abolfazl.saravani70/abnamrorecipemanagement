package com.abnamro.recipemanagement.index.dto.search;

import com.abnamro.recipemanagement.model.Recipe;
import org.hibernate.search.engine.search.predicate.SearchPredicate;
import org.hibernate.search.mapper.orm.scope.SearchScope;

public class IngredientsSearchParameter extends SearchParameter<String,Recipe>{



    public IngredientsSearchParameter(String name,String value, Boolean mustHold) {
        this.name = name;
        this.value = value;
        this.mustHold = mustHold;
    }

    @Override
    public SearchPredicate applyFilter(SearchScope<Recipe> searchScope ) {
        return searchScope.predicate().nested().objectField("ingredients").nest(x->x.match().field("ingredients."+getName()).matching(getValue())).toPredicate();
    }


}
