package com.abnamro.recipemanagement.index.dto;

import com.abnamro.recipemanagement.model.Ingredient;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class IngredientDTO {
    private Integer id;

    private String name;

    public void map(Ingredient ingredient){
        this.setId(ingredient.getId());
        this.setName(ingredient.getName());
    }
}
