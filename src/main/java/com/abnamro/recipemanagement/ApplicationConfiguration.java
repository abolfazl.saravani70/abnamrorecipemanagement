package com.abnamro.recipemanagement;

import com.abnamro.recipemanagement.index.Indexer;
import com.abnamro.recipemanagement.model.SearchRepositoryImpl;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = SearchRepositoryImpl.class,basePackages = {"com.abnamro.recipemanagement.repository"})
public class ApplicationConfiguration {
    @Bean
	public ApplicationRunner buildIndex(Indexer indexer) {
		return (ApplicationArguments args) -> {
			indexer.indexPersistedData("com.abnamro.recipemanagement.model.Recipe");
			indexer.indexPersistedData("com.abnamro.recipemanagement.model.Ingredient");
		};
	}
}
