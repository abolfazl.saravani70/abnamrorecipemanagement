package com.abnamro.recipemanagement.model;


import com.abnamro.recipemanagement.index.dto.RecipeDTO;
import lombok.*;
import org.hibernate.search.engine.backend.types.ObjectStructure;
import org.hibernate.search.engine.backend.types.Projectable;
import org.hibernate.search.engine.backend.types.Searchable;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.GenericField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexedEmbedded;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Indexed
@Entity
@Table(name = "recipe")
@Setter
@Getter
@RequiredArgsConstructor
public class Recipe {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "servingSize")
    @GenericField(searchable = Searchable.YES,projectable = Projectable.YES)
    private Integer servingSize;

    @FullTextField
    @Column(name = "name")
    private String name;

    @FullTextField
    @Column(name = "instruction",length = 500)
    private String instruction;

    @Column(name = "isVegetarian")
    @GenericField
    private Boolean isVegetarian;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "recipe_ingredient",
            joinColumns = @JoinColumn(name = "recipe_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "ingredient_id", referencedColumnName = "id")
    )
    @IndexedEmbedded(structure = ObjectStructure.NESTED)
    private List<Ingredient> ingredients = new ArrayList<>();


    public void map(RecipeDTO dto){
        this.setId(dto.getId());
        this.setName(dto.getName());
        this.setInstruction(dto.getInstruction());
        this.setIsVegetarian(dto.getIsVegetarian());
        this.setServingSize(dto.getServingSize());


    }

}
