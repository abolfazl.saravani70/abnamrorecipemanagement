package com.abnamro.recipemanagement.model;


import com.abnamro.recipemanagement.index.dto.IngredientDTO;
import lombok.*;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Indexed
@Table(name = "ingredient")
@Setter
@Getter
@RequiredArgsConstructor
public class Ingredient {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "name",nullable = false)
    @FullTextField
    private String name;

   @ManyToMany(mappedBy = "ingredients")
   private List<Recipe> recipes = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        return name.equalsIgnoreCase(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void map(IngredientDTO ingredientDTO){
        this.setName(ingredientDTO.getName());
        this.setId(ingredientDTO.getId());
    }
}
