package com.abnamro.recipemanagement.model;

import com.abnamro.recipemanagement.index.dto.search.SearchParameter;
import com.abnamro.recipemanagement.repository.SearchRepository;
import org.hibernate.search.engine.search.predicate.dsl.BooleanPredicateClausesStep;
import org.hibernate.search.engine.search.query.SearchResult;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.scope.SearchScope;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

@Transactional
public class SearchRepositoryImpl<T , ID extends Serializable> extends SimpleJpaRepository<T, ID>
        implements SearchRepository<T, ID> {

    final EntityManager entityManager;


    public SearchRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
        super(domainClass, entityManager);
        this.entityManager = entityManager;
    }

    public SearchRepositoryImpl(
            JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

//    @Override
//    public SearchResult<T> searchBy(String text, int limit, String... fields) {
//        SearchSession searchSession = Search.session(entityManager);
//        SearchResult<T> result =
//                searchSession
//                        .search(getDomainClass())
//                        .where(f -> f.match().fields(fields).matching(text).fuzzy(2))
//                        .fetch(limit);
//        return result;
//    }

    public SearchResult<T> multipleFiltersSearch(List<SearchParameter> searchParameters, int limit){
        SearchSession searchSession = Search.session(entityManager);
        SearchScope<T> searchScope = searchSession.scope(getDomainClass());

        BooleanPredicateClausesStep<?> finalFilter = searchScope.predicate().bool();
        for (SearchParameter parameter : searchParameters) {
            if(parameter.getMustHold()) finalFilter = finalFilter.must(parameter.applyFilter(searchScope));
            else finalFilter = finalFilter.mustNot(parameter.applyFilter(searchScope));
        }

        SearchResult<T> results = searchSession
                .search(searchScope)
                .where(finalFilter.toPredicate())
                .fetch(limit);

        return results;
    }

}
