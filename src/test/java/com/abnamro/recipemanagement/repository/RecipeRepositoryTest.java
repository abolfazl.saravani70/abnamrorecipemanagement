package com.abnamro.recipemanagement.repository;

import com.abnamro.recipemanagement.index.IndexException;
import com.abnamro.recipemanagement.index.Indexer;
import com.abnamro.recipemanagement.index.dto.search.BooleanSearchParameter;
import com.abnamro.recipemanagement.index.dto.search.StringSearchParameter;
import com.abnamro.recipemanagement.model.Ingredient;
import com.abnamro.recipemanagement.model.Recipe;
import com.abnamro.recipemanagement.index.dto.search.IntegerSearchParameter;
import org.hibernate.search.mapper.orm.Search;
import org.hibernate.search.mapper.orm.massindexing.MassIndexer;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;



@AutoConfigureTestEntityManager
@AutoConfigureTestDatabase
@SpringBootTest
@Transactional
class RecipeRepositoryTest {

    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private EntityManager entityManager;
    /**
     * Adding 3 recipes and 3 Ingredients.
     */
    @BeforeEach
    @Transactional
    public void initUseCase(){
    }

    @AfterEach
    void deleteAll(){
        recipeRepository.deleteAll();
        ingredientRepository.deleteAll();
    }
    @Test
    void WhenSearchRecipeByServingSizeOneReturnBoiledEggRecipe(){
        //Data using application inserted data in Application.class

        //Logic
        IntegerSearchParameter integerSearchParameter = new IntegerSearchParameter("servingSize",1,true);
        StringSearchParameter stringSearchParameter = new StringSearchParameter("instruction","eggs",true);
        BooleanSearchParameter booleanSearchParameter = new BooleanSearchParameter("IsVegetarian",true,true);
        List<Recipe> searchResult = recipeRepository.multipleFiltersSearch(Arrays.asList(integerSearchParameter,stringSearchParameter),10).hits();

        //Assert

        assertTrue(searchResult.size()==1);
        assertTrue(searchResult.get(0).getName().equals("Boiled Eggs"));
    }
    /**
     * Creating Three recipes and Three ingredients
     * @return
     */
    List<Recipe> fillSampleRecipeList(){

        List<Recipe> recipes = new ArrayList<>(3);
        Recipe recipeOne = new Recipe();
        recipeOne.setName("Boiled Eggs");
        recipeOne.setInstruction("Place your eggs in a pot and cover with cold water by 1 inch. Bring to a boil over medium-high heat, then cover, remove from the heat and set aside 8 to 10 minutes. Drain, cool in ice water and peel.");
        recipeOne.setServingSize(1);
        recipeOne.setIsVegetarian(true);

        Recipe recipeTwo = new Recipe();
        recipeTwo.setName("Egg Salad");
        recipeTwo.setServingSize(2);
        recipeTwo.setIsVegetarian(true);
        recipeTwo.setInstruction("Place egg in a saucepan and cover with cold water. Bring water to a boil and immediately remove from heat. Cover and let eggs stand in hot water for 10 to 12 minutes. Remove from hot water, cool, peel and chop. " +
                "Place the chopped eggs in a bowl, and stir in the mayonnaise, mustard and green onion. Season with salt, pepper and paprika. Stir and serve on your favorite bread or crackers.");


        Recipe recipeThree = new Recipe();
        recipeThree.setName("Raw chicken");
        recipeThree.setServingSize(4);
        recipeThree.setIsVegetarian(false);
        recipeThree.setInstruction("Defrost the chicken you freshly bought from AH Jumbo. Wash the chicken thoroughly. Then eat one bite at a time with a disgusted face.");



        Ingredient ingredientOne = new Ingredient();
        ingredientOne.setName("Egg");

        Ingredient ingredientTwo = new Ingredient();
        ingredientTwo.setName("Lattice");
        Ingredient ingredientThree = new Ingredient();
        ingredientThree.setName("Chicken");

        recipeOne.getIngredients().add(ingredientOne);
        recipeTwo.getIngredients().add(ingredientOne);
        recipeTwo.getIngredients().add(ingredientTwo);
        recipeThree.getIngredients().add(ingredientThree);

        recipes.add(recipeOne);
        recipes.add(recipeTwo);
        recipes.add(recipeThree);

        return recipes;
    }

    @Test
    void WhenSaveAllCheckValidIds(){
        //Data
        List<Recipe> recipes = fillSampleRecipeList();

        //Logic
        List<Recipe> result= recipeRepository.saveAllAndFlush(recipes);

        //Assert
        AtomicInteger validIdCounter = new AtomicInteger(0);

        result.forEach(recipe -> {
            if(recipe.getId()>0 && recipe.getId()!=null) validIdCounter.getAndIncrement();
        });

        assertTrue(validIdCounter.get()==3);
    }

    @Test
    void WhenSaveAllCheckPersistCascadeToIngredients(){
        //Data
        List<Recipe> recipes = fillSampleRecipeList();

        //Logic
        recipeRepository.saveAllAndFlush(recipes);
        List<Ingredient> ingredients = ingredientRepository.findAll();

        //Assert
        assertTrue(ingredients.size()==6);

    }

    @Test
    void WhenAddedNewRecipeWithPreviousIngredientCheckPersisCascade(){
        //Data
        List<Recipe> recipes = fillSampleRecipeList();
        recipeRepository.saveAllAndFlush(recipes);
        List<Ingredient> ingredients = ingredientRepository.findAll();

        //Logic
        Recipe newRecipe = new Recipe();
        newRecipe.getIngredients().addAll(ingredients);
        recipeRepository.saveAndFlush(newRecipe);
        List<Ingredient> newIngredientsList = ingredientRepository.findAll();

        //Assert
        assertTrue(newIngredientsList.size()==6);

    }

    @Test
    void WhenDeleteRecipeIngredientsStay(){
        //Data
        List<Recipe> recipes = fillSampleRecipeList();
        recipeRepository.saveAllAndFlush(recipes);
        //Logic
        recipeRepository.deleteAll();
        recipeRepository.flush();
        List<Ingredient> ingredients = ingredientRepository.findAll();

        //Assert
        assertTrue(ingredients.size()==6);
    }

    @Test
    void WhenUpdateRecipeCheckMergeCascadeToIngredients(){
        //Data
        List<Recipe> recipes = fillSampleRecipeList();
        recipes = recipeRepository.saveAllAndFlush(recipes);

        //Logic
        List<Ingredient> ingredients = ingredientRepository.findAll();
        recipes.get(0).getIngredients().add(ingredients.get(0));
        recipeRepository.saveAndFlush(recipes.get(0));

        //Assert
        ingredients = ingredientRepository.findAll();
        assertTrue(ingredients.size()==6);
    }



}