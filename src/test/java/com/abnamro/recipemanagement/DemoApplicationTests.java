package com.abnamro.recipemanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {


	/**
	 * Testing if the project context loads. specially to chech if dependancy injection is working and we have no loops.
	 */
	@Test
	void contextLoads() {
	}

}
