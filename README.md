ABN AMRO Recipe Management

Dear Reader (Probably Pleun),

Project Context

I have created a Spring Boot project wit REST Apis to create, delete, update and search Recipes. Each Recipe has a name, instruction, servingsize and whether it is vegarian. 
Ingredients have names and Id.
You can search recipes by name, vegerian, ingredients, servingsize or a word inside instructions. I assumed this is a REST api working with a UI written in something like an Angular.

Development Context

Last time I used SpringBoot was quite a while. I did not get a change to use during my projects at TU/e. I re-read the concise book of Craig Walls, "Spring Boot in Action" and then I tried to use Hibernate text search with Lucene.
Hibernate has changed alot in version 6. It was my first time using the Lucene search. I did also include some tests but for some reason, no matter how much I tried, adding new data inside the test class and indexing it did not work.
I know I should have added more tests for my other layers but I am in a tight schdule right now. 
I realize by the time I want to send this that maybe the reason you asked the code to be uploaded to a git repository is that you wanted to see how I commit as well?
If so, I commit at small intervalls in a way that I describe my commit in a few sentences.

Final Thoughts

I do appreciate the time you put into this. I can imagine how tiresome it is to read this type of sample codes. I am kinda of stuck between technologies right now.
But I am re-reading and watching new material everyday. I want to get back to Java and its related technologies.


